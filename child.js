'use strict';

/**
 * Loading external module
 */
const cluster = require('cluster')
const express = require('express')
const bodyParser = require('body-parser')

/**
 *
 */
class ChildApp {

  constructor () {
    this.app = express()
  }

  run (port) {
    console.log(`Starting app with pid : ${process.pid} and http:${port}`)

    this.app.use(bodyParser.urlencoded({
      extended: true
    }))
    this.app.use(bodyParser.json())

    this.app.get('/', (req, res) => {
      res.status(200)
      res.json({
        message: `Welcome to API cluster with pid : ${process.pid} and http:${port}`
      })
    })

    this.app.listen(port, () => {
      console.log(`Express running to ${port} in process : ${process.pid}`)
    })
  }

}

module.exports = ChildApp;
