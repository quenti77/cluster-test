'use strict';

/**
 * Loading external module
 */
const cluster = require('cluster')

/**
 * Loading internal module
 */
const Master = require('./master')
const Child = require('./child')

if (cluster.isMaster) {
  const masterProcess = new Master()
  masterProcess.run()
} else {
  const HTTP_PORT = parseInt(process.env.HTTP_PORT) || 1337
  const childProcess =  new Child()

  childProcess.run(HTTP_PORT)
}
