'use strict';

/**
 * Loading external module
 */
const cluster = require('cluster')

/**
 *
 */
class Master {

  constructor () {
    this.workers = []
  }

  run () {
    console.log(`Creating process ...`)
    for (let i = 0; i < 4; i += 1) {
      this.workers.push(cluster.fork())
    }
    console.log(`Number of cluster : ${this.workers.length}`)
  }

}

module.exports = Master;
